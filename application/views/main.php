<!DOCTYPE html> <!-- Определяем Doctype для корректного отображения -->
<html>
    <head>
        <title>Dimetrix</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="<?= URL::base(); ?>/assets/css/main.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <form id="registration" class="form-horizontal" action="registration" method="POST" novalidate>

            <fieldset>
                <!-- Form Name -->        
                <legend>
                    <div class="form-group">
                        <div class="col-md-4 control-label"></div>
                        <div class="col-md-6">
                            Форма регистрации
                        </div>
                    </div>
                </legend>

                <!-- Email input-->
                <div class="form-group control-group">
                    <label class="col-md-4 control-label" for="email">Адрес электронной почты</label>  
                    <div class="col-md-6 controls">
                        <input data-validation-required-message="Поле не должно быть пустым" required="" data-validation-email-message="Введите валидный адрес электронной почты" id="email" name="email" type="email" placeholder="Адрес электронной почты" class="form-control input-md">               
                        <div class="help-block"></div>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group control-group">
                    <label class="col-md-4 control-label" for="password">Пароль</label>
                    <div class="col-md-6 controls">
                        <input data-validation-required-message="Поле не должно быть пустым" required="" data-validation-minlength-message="Минимальная длина '8' символов" id="password" name="password" type="password" placeholder="Пароль" class="form-control input-md" minlength="8">
                        <div class="limit">
                            Ваш пароль подберут за <span id="time">секунду</span>
                        </div>
                        <div class="help-block"></div>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group control-group">
                    <label class="col-md-4 control-label" for="confirm">Подтверждение пароля</label>
                    <div class="col-md-6 controls">
                        <input data-validation-required-message="Поле не должно быть пустым" data-validation-match-match="password" data-validation-match-message="Пароли не совпадают" id="confirm" name="confirm" type="password" placeholder="Подтверждение пароля" class="form-control input-md">
                        <div class="help-block"></div>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="send"></label>
                    <div class="col-md-4 form-actions">
                        <button class="btn btn-primary" type="submit">Регистрация<i class="icon-ok icon-white"></i></button>
                    </div>
                </div>
            </fieldset>
        </form>

        <script src="http://code.jquery.com/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

        <script src="http://malsup.github.com/jquery.form.js"></script> 

        <script src="<?= URL::base(); ?>/assets/js/bootbox.min.js"></script>

        <script src="<?= URL::base(); ?>/assets/js/jqBootstrapValidation.js"></script>

        <script src="<?= URL::base(); ?>/assets/js/jquery.pwdstr-1.0.source.js"></script>

        <script src="<?= URL::base(); ?>/assets/js/main.js"></script>

    </body>
</html>