﻿<?php

class Model_User extends Model {

    public function create($array) {
        DB::insert('user', array('email', 'password'))
                ->values(array($array['email'], md5($array['password'])))
                ->execute();
    }

    public function unique_email($email) {
        return 0 == DB::select(array(DB::expr('COUNT(email)'), 'total'))
                        ->from('user')
                        ->where('email', '=', $email)
                        ->execute()
                        ->get('total');
    }

}
