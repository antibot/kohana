﻿<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Template {

    public $template = 'main';

    /**
     * Действие по умолчанию
     */
    public function action_index() {
        
    }

    /**
     * Регистрация 
     */
    public function action_registration() {
        $this->auto_render = false;

        $post = Validation::factory($_POST);

        $post->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('password', 'not_empty')
                ->rule('password', 'min_length', array(':value', '8'))
                ->rule('confirm', 'not_empty')
                ->rule('confirm', 'min_length', array(':value', '8'))
                ->rule('confirm', 'matches', array(':validation', 'confirm', 'password'));
        //->rule('email', 'Model_User::unique_email');

        if ($post->check()) {
            $email = $_POST['email'];

            $user = Model::factory('user');

            if ($user->unique_email($email)) {
                try {
                    $user->create($post);

                    $to = $email; 

                    $title = 'Регистрация прошла успешно!';

                    $message = 'https://bitbucket.org/antibot/kohana'; 

                    $from = 'hi@dimetrix.ru';

                    mail($to, $title, $message, 'From:' . $from);
                } catch (Exception $exc) {
                    
                }

                echo json_encode(array('error' => 'none'));
            } else {
                echo json_encode(array('error' => 'email'));
            }
        } else {
            echo json_encode(array('error' => implode(',', array_keys($post->errors()))));
        }
    }

    /**
     * Авторизация
     */
    public function action_authorization() {
        $this->auto_render = false;
    }

}

// End Main
