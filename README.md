Добрый день! :)

1) Пулим репозитарий в любую папку доступную для сервера
2) Открываем файл application\bootstrap.php и находим там 
Kohana::init(array(
    'base_url' => '/kohana/'
));
'base_url' - базовый адрес сайта. Например, если сайт находится в 'http://localhost/kohana/', то base_url равен 'kohana'. 
3) Для сервера Apache находим .htaccess и меняем RewriteBase /kohana/ по правилам пункта 3,
для сервера nginx аккуратно скопировать верхнии настройки nginx.txt
4) Открываем файл application\config\database.php и прописываем настройки подключения к базе
5) Импортируем дамп базы kohana.sql
