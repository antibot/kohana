/** Композиция слова и окончани[е, я, ий]
 * 
 * @param value - число
 * @param result - слово без окончания
 * @param arg1
 * @param arg2
 * @param arg3
 * @return 
 * 
 */
function composition(value, result, arg1, arg2, arg3) {
    arg1 = "" || arg1;
    arg2 = "" || arg2;
    arg3 = "" || arg3;

    var count = value;

    var last_digit = count % 10;
    var last_two_digits = count % 100;

    if (last_digit === 1 && last_two_digits !== 11) {
        result += arg1;
    } else if ((last_digit === 2 && last_two_digits !== 12)
            || (last_digit === 3 && last_two_digits !== 13)
            || (last_digit === 4 && last_two_digits !== 14)) {
        result += arg2;
    } else {
        result += arg3;
    }

    return result;
}


(function($) {
    $.fn.extend({
        pwdstr: function(el) {
            return this.each(function() {



                $(this).keyup(function() {
                    $(el).html(getTime($(this).val()));
                });

                function getTime(str) {

                    var chars = 0;
                    var rate = 2800000000;

                    if ((/[a-z]/).test(str))
                        chars += 26;
                    if ((/[A-Z]/).test(str))
                        chars += 26;
                    if ((/[0-9]/).test(str))
                        chars += 10;
                    if ((/[^a-zA-Z0-9]/).test(str))
                        chars += 32;

                    var pos = Math.pow(chars, str.length);
                    var s = pos / rate;

                    var decimalYears = s / (3600 * 24 * 365);
                    var years = Math.floor(decimalYears);

                    var decimalMonths = (decimalYears - years) * 12;
                    var months = Math.floor(decimalMonths);

                    var decimalDays = (decimalMonths - months) * 30;
                    var days = Math.floor(decimalDays);

                    var decimalHours = (decimalDays - days) * 24;
                    var hours = Math.floor(decimalHours);

                    var decimalMinutes = (decimalHours - hours) * 60;
                    var minutes = Math.floor(decimalMinutes);

                    var decimalSeconds = (decimalMinutes - minutes) * 60;
                    var seconds = Math.floor(decimalSeconds);

                    var time = [];

                    if (years > 0) {
                        if (years === 1)
                            time.push("1 год, ");
                        else
                            time.push(years + composition(years, " ", "год", "года", "лет") + ", ");
                    }
                    if (months > 0) {
                        if (months === 1)
                            time.push("1 месяц, ");
                        else
                            time.push(months + composition(months, " месяц", "", "а", "ев") + ", ");
                    }
                    if (days > 0) {
                        if (days === 1)
                            time.push("1 день, ");
                        else
                            time.push(days + composition(days, " ", "день", "дня", "дней") + ", ");
                    }
                    if (hours > 0) {
                        if (hours == 1)
                            time.push("1 час, ");
                        else
                            time.push(hours + composition(hours, " час", "", "а", "ов") + ", ");
                    }
                    if (minutes > 0) {
                        if (minutes == 1)
                            time.push("1 минуту, ");
                        else
                            time.push(minutes + composition(minutes, " минут", "у", "ы", "") + ", ");
                    }
                    if (seconds > 0) {
                        if (seconds === 1)
                            time.push("1 секунду, ");
                        else
                            time.push(seconds + composition(seconds, " секунд", "у", "ы", "") + ", ");
                    }

                    if (time.length <= 0)
                        time = "секунду, ";
                    else if (time.length === 1)
                        time = time[0];
                    else
                        time = time[0] + time[1];

                    return time.substring(0, time.length - 2);
                }

            });
        }
    });
})(jQuery); 