﻿$(function() {

    $('input, select, textarea').not('[type=submit]').jqBootstrapValidation({
        preventSubmit: true,
        submitError: function(form, event, errors) {
            console.log('errors', errors);
        },
        submitSuccess: function(form, event) {

        },
        filter: function() {
            return $(this).is(':visible');
        }
    });

    $('input[name="password"]').pwdstr('#time');

    $('#registration').ajaxForm({
        target: '#output', // target element(s) to be updated with server response 
        beforeSubmit: function() {

        },
        beforeSerialize: function() {

        },
        success: function(data) {
            console.log('data', data);
            
            data = $.parseJSON($.trim(data));

            if (data && data.hasOwnProperty('error')) {
                if (data.error === 'none') {
                    bootbox.alert('<b>Регистрация прошла успешно!</b><br />Вам выслано письмо с дальнейшими инструкциями!');
                } else {
                    var errors = data.error.split(',');
                    var messages = [];

                    $.each(errors, function(index, error) {
                        switch (error) {
                            case 'email':
                                messages.push('Пользователь с такой почтой уже существует!');
                                break;
                            case 'password':
                                messages.push('Пароль слишком мал!');
                                break;
                            case 'confirm':
                                break;
                                messages.push('Пароли не совпадают!');
                            default:
                                messages.push('Неизвестная ошибка');
                                break;
                        }
                    });

                    if (messages.length > 0) {
                        bootbox.alert('<b>Ошибка регистрации!</b><br />' + messages.join('<br />') + '<br />Попробуйте еще раз!');
                    }
                }
            } else {
                bootbox.alert('<b>Ошибка регистрации!</b><br />Попробуйте еще раз!');
            }
        }, // post-submit success 
        error: function(error) {
            console.log('error', error);
        }, // post-submit error 
        clearForm: false, // clear all form fields after successful submit 
        resetForm: false, // reset the form after successful submit 
        dataType: 'text'
    });
});